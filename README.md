# Ancile Microsoft Sky Drive
### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_MSSkyDrive

This plugin disables Microsoft Sky Drive's file synchronization. 

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile